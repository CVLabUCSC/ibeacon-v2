//
//  MViewController.m
//  ATest
//
//  Created by Roberto Manduchi on 6/13/14.
//  Copyright (c) 2014 Roberto Manduchi. All rights reserved.
//

#import "MViewController.h"
#import <FYX/FYXVisitManager.h>
#import <FYX/FYXTransmitter.h>
#import <FYX/FYX.h>
#import <FYX/FYXiBeacon.h>

@interface MViewController ()

@end

@implementation MViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    
    [FYX startService:self];
    self.visitManager = [FYXVisitManager new];
    self.visitManager.delegate = self;
    self.visitManager.iBeaconDelegate = self;
    [self.visitManager start];
    

}
- (void)serviceStarted
{
    // this will be invoked if the service has successfully started
    // bluetooth scanning will be started at this point.
    NSLog(@"FYX Service Successfully Started");
    }
- (void)startServiceFailed:(NSError *)error
{
    // this will be called if the service has failed to start
    NSLog(@"%@", error);
}

/*
- (void)didArrive:(FYXVisit *)visit;
{
    // this will be invoked when an authorized transmitter is sighted for the first time
    NSLog(@"I arrived at a Gimbal Beacon!!! %@", visit.transmitter.name);
    NSString *preamble =  @"I arrived at a Gimbal Beacon!!!";
    self.logText.text = [preamble stringByAppendingString:visit.transmitter.name];
    

}
- (void)receivedSighting:(FYXVisit *)visit updateTime:(NSDate *)updateTime RSSI:(NSNumber *)RSSI;
{
    // this will be invoked when an authorized transmitter is sighted during an on-going visit
    NSLog(@"I received a sighting!!! %@", visit.transmitter.name);
    self.logText.text = [NSString stringWithFormat:@"I received a sighting!!! %@", visit.transmitter.name];
    
}
- (void)didDepart:(FYXVisit *)visit;
{
    // this will be invoked when an authorized transmitter has not been sighted for some time
    self.logText.text = [NSString stringWithFormat:@"I left the proximity of a Gimbal Beacon!!! %@", visit.transmitter.name, '\n',@"I was around the beacon for %f seconds", visit.dwellTime];
    
    NSLog(@"I left the proximity of a Gimbal Beacon!!!! %@", visit.transmitter.name);
    NSLog(@"I was around the beacon for %f seconds", visit.dwellTime);
}
 */

- (void)didArriveIBeacon:(FYXiBeaconVisit *)visit;
{
    // this will be invoked when a managed Gimbal beacon is sighted for the first time
    NSLog(@"I arrived within the proximity of a Gimbal beacon!!! Proximity UUID:%@ Major:%@ Minor:%@", visit.iBeacon.uuid, visit.iBeacon.major, visit.iBeacon.minor);
    NSString *preamble =  @"I arrived at a Gimbal Beacon!!!";
    self.logText.text = [preamble stringByAppendingString:visit.iBeacon.identifier];
}
- (void)receivedIBeaconSighting:(FYXiBeaconVisit *)visit updateTime:(NSDate *)updateTime RSSI:(NSNumber *)RSSI;
{
    // this will be invoked when a managed Gimbal beacon is sighted during an on-going visit
    NSLog(@"I received a sighting!!! Proximity UUID:%@ Major:%@ Minor:%@", visit.iBeacon.uuid, visit.iBeacon.major, visit.iBeacon.minor);
    self.logText.text = [NSString stringWithFormat:@"I received a sighting!!! %@", visit.iBeacon.identifier];

}
- (void)didDepartIBeacon:(FYXiBeaconVisit *)visit;
{
    // this will be invoked when a managed Gimbal beacon has not been sighted for some time
    NSLog(@"I left the proximity of a Gimbal beacon!!!! Proximity UUID:%@ Major:%@ Minor:%@", visit.iBeacon.uuid, visit.iBeacon.major, visit.iBeacon.minor);
    NSLog(@"I was around the beacon for %f seconds", visit.dwellTime);
    self.logText.text = [NSString stringWithFormat:@"I left the proximity of a Gimbal Beacon!!! %@", visit.iBeacon.identifier, '\n',@"I was around the beacon for %f seconds", visit.dwellTime];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
