//
//  main.m
//  ATest
//
//  Created by Roberto Manduchi on 6/13/14.
//  Copyright (c) 2014 Roberto Manduchi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MAppDelegate class]));
    }
}
