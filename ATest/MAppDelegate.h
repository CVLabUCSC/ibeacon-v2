//
//  MAppDelegate.h
//  ATest
//
//  Created by Roberto Manduchi on 6/13/14.
//  Copyright (c) 2014 Roberto Manduchi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
