//
//  MViewController.h
//  ATest
//
//  Created by Roberto Manduchi on 6/13/14.
//  Copyright (c) 2014 Roberto Manduchi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FYX/FYX.h>
#import <FYX/FYXVisitManager.h>

@interface MViewController : UIViewController<FYXServiceDelegate>

@property (weak, nonatomic) IBOutlet UILabel *logText;

@property (nonatomic) FYXVisitManager *visitManager;

@end

